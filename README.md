Author: [Robinson Dias](https://github.com/robinson-1985/)

# app-cadastro-de-series-dotnet-dio

## Bootcamp Decola Tech - Digital Innovation One.

- Nesse projeto aprendemos como criar um algoritmo simples de cadastro de séries para praticar os nossos
seus conhecimentos de orientação a objetos, o principal paradigma de programação utilizada no mercado. 
Assim, aprendemos: Como pensar orientado a objetos, como modelar o seu domínio, como utilizar recursos de 
coleção para salvar seus dados em memória.

-Para iniciar o projeto:

```dotnet new console -n DIO.Series```

- Expert especialista da Dio: Eliézer Zarpelão
 
- Link do curso: (https://web.digitalinnovation.one/lab/criando-um-app-de-cadastro-em-memoria-implementando-crud-de-series-em-net/learning/a8f22dd2-9c2d-4e87-b46c-15602451ef9b)


